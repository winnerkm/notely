package com.phonepay.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonepay.R;
import com.phonepay.adaptor.NoteModel;
import com.phonepay.database.Note;
import com.phonepay.fragments.LandingFragment;
import com.phonepay.utils.Constants;
import com.phonepay.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LandingActivity extends Activity implements LandingFragment.OnFragmentInteractionListener {

    private NoteModel noteModel;

    @BindView(R.id.navigation_view) NavigationView mNavigationView;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;

    /* ************* Side Menu *********************/

    @BindView(R.id.fav_tv) TextView favTV;
    @BindView(R.id.stared_tv) TextView staredTV;
    @BindView(R.id.poem_tag_tv) TextView poemTagTV;
    @BindView(R.id.story_tag_tv) TextView storyTagTV;
    @BindView(R.id.poem_tag_iv) ImageView poemTagIV;
    @BindView(R.id.story_tag_iv) ImageView storyTagIV;
    @BindView(R.id.fav_iv) ImageView favIV;
    @BindView(R.id.stared_iv) ImageView staredIV;

    private boolean isFav = false;

    @OnClick(R.id.fav_container)
    void onFavFilterClick() {
        isFav = !isFav;
        if (isFav) {
            highlightFilter(favTV, favIV);
        } else {
            unHighlightFilter(favTV, favIV);
        }
    }

    private boolean isStared = false;

    @OnClick(R.id.stared_container)
    void onHeartedFilterClick() {
        isStared = !isStared;

        if (isStared) {
            highlightFilter(staredTV, staredIV);
        } else {
            unHighlightFilter(staredTV, staredIV);
        }

    }

    private boolean isPoem;

    @OnClick(R.id.poem_tag_container)
    void onPoemFilterClick() {
        isPoem = !isPoem;

        if (isPoem) {
            highlightFilter(poemTagTV, poemTagIV);
        } else {
            unHighlightFilter(poemTagTV, poemTagIV);
        }

    }

    private boolean isStory;

    @OnClick(R.id.story_tag_container)
    void onStoryFilterClick() {
        isStory = !isStory;

        if (isStory) {
            highlightFilter(storyTagTV, storyTagIV);
        } else {
            unHighlightFilter(storyTagTV, storyTagIV);
        }

    }

    private int mTagType;
    private int mLoadDataFlag;

    @OnClick(R.id.apply_btn)
    void onApplyBtnClick() {
        if (isStared && isFav) {
            mLoadDataFlag = Constants.FAV_AND_STARED_NOTES;
        } else if (isFav) {
            mLoadDataFlag = Constants.FAV_NOTES;
        } else if (isStared) {
            mLoadDataFlag = Constants.STARED_NOTES;
        } else {
            mLoadDataFlag = Constants.ALL_NOTES;
        }

        if (isPoem && isStory) {
            mTagType = Constants.POEM_AND_STORY_TAG_NOTES;
        } else if (isPoem) {
            mTagType = Constants.POEM_TAG_NOTES;
        } else if (isStory) {
            mTagType = Constants.STORY_TAG_NOTES;
        } else {
            mTagType = Constants.TAG_NONE;
        }

        onDrawerSlide();
        openFragment();
    }

    void highlightFilter(TextView textView, ImageView imageView) {
        textView.setTextColor(getResources().getColor(R.color.green));
        imageView.setImageResource(R.drawable.ic_done_green);
    }

    void unHighlightFilter(TextView textView, ImageView imageView) {
        textView.setTextColor(getResources().getColor(R.color.gray));
        imageView.setImageResource(R.drawable.ic_done);
    }

    /********************************************/


    @OnClick(R.id.add_note_btn)
    public void onAddNoteClick(View view) {
        launchNoteActivity(Constants.CREATE);
    }

    @OnClick({R.id.drawer_btn, R.id.filter_close_btn})
    void onDrawerBtnClick() {
        onDrawerSlide();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        openFragment();
    }

    public void openFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = new LandingFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.LOAD_DATA_FLAG, mLoadDataFlag);
        bundle.putInt(Constants.FILTER_TAG_, mTagType);
        fragment.setArguments(bundle);
        fragmentTransaction.add(R.id.content_frame, fragment, LandingFragment.TAG).commit();
    }

    private void launchNoteActivity(String frag_type) {
        Intent intent = new Intent(LandingActivity.this, NoteActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Constants.FRAG_MODE, false);
        intent.putExtra(Constants.FRAG_TYPE, frag_type);
        intent.putExtra(Constants.NOTE_MODEL, noteModel);
        startActivity(intent);
    }

    @Override
    public void openDisplayFrag(Note note) {
        noteModel = Utilities.convertNoteData(note);
        launchNoteActivity(Constants.DISPLAY);
    }

    @Override
    public void openCreateFrag() {
        launchNoteActivity(Constants.CREATE);
    }

    public void onDrawerSlide() {
        if (mDrawerLayout.isDrawerOpen(Gravity.END)) {
            mDrawerLayout.closeDrawer(Gravity.END);
        } else {
            mDrawerLayout.openDrawer(Gravity.END);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.END)) {
            mDrawerLayout.closeDrawer(Gravity.END);
        } else {
            super.onBackPressed();
        }
    }
}
