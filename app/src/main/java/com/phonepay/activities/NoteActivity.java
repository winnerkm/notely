package com.phonepay.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.phonepay.R;
import com.phonepay.adaptor.NoteModel;
import com.phonepay.database.AppDatabase;
import com.phonepay.database.Note;
import com.phonepay.fragments.NoteCreationFragment;
import com.phonepay.fragments.NoteDisplayFragment;
import com.phonepay.utils.Constants;
import com.phonepay.utils.Utilities;

import butterknife.BindView;

public class NoteActivity extends Activity implements NoteCreationFragment.OnFragmentInteractionListener,
        NoteDisplayFragment.OnFragmentInteractionListener {

    @BindView(R.id.content_frame)
    FrameLayout frameLayout;

    private AppDatabase db;
    private boolean isEditMode;
    private String frag_type;
    private NoteModel mNoteModel;
    private Note mNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        db = Room.databaseBuilder(this, AppDatabase.class, AppDatabase.DB_NAME).build();

        if (getIntent().hasExtra(Constants.FRAG_TYPE)) {
            frag_type = getIntent().getStringExtra(Constants.FRAG_TYPE);
        }

        if (getIntent().hasExtra(Constants.FRAG_MODE)) {
            isEditMode = getIntent().getBooleanExtra(Constants.FRAG_MODE, false);
        }
        if (getIntent().hasExtra(Constants.NOTE_MODEL)) {
            mNoteModel = getIntent().getParcelableExtra(Constants.NOTE_MODEL);
        }

        launchFragment();
    }

    private void launchFragment() {

        if (frag_type.equalsIgnoreCase(Constants.DISPLAY)) {
            openDisplayFragment();
        } else {
            openNoteCreationFragment();
        }

    }

    public void openDisplayFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = fragmentManager.findFragmentByTag(NoteDisplayFragment.TAG);

        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.NOTE_MODEL, mNoteModel);

        if (fragment == null) {
            fragment = new NoteDisplayFragment();
            fragment.setArguments(bundle);
            fragmentTransaction.add(R.id.content_frame, fragment, NoteDisplayFragment.TAG).commit();
        } else {
            fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.content_frame, fragment, NoteDisplayFragment.TAG).commit();
        }

    }

    public void openNoteCreationFragment() {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = fragmentManager.findFragmentByTag(NoteCreationFragment.TAG);

        Bundle bundle = new Bundle();

        if (isEditMode) {
            bundle.putParcelable(Constants.NOTE_MODEL, mNoteModel);
            bundle.putBoolean(Constants.FRAG_MODE, true);
        }

        if (fragment == null) {
            fragment = new NoteCreationFragment();
            fragment.setArguments(bundle);
            fragmentTransaction.add(R.id.content_frame, fragment, NoteCreationFragment.TAG).commit();
        } else {
            fragmentTransaction.replace(R.id.content_frame, fragment, NoteCreationFragment.TAG).commit();
        }

    }

    @Override
    public void onEditClick(NoteModel noteModel) {
        isEditMode = true;
        mNoteModel = noteModel;
        openNoteCreationFragment();
    }

    @SuppressLint("StaticFieldLeak")
    private class DatabaseAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            if (isEditMode) {
                db.noteDao().updateRecord(mNote);
            } else {
                db.noteDao().insertNote(mNote);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }

    @Override
    public void saveData(Note note, boolean editMode) {
        isEditMode = editMode;
        mNote = note;
        mNoteModel = Utilities.convertNoteData(note);
        new DatabaseAsync().execute();
    }

}
