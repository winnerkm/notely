package com.phonepay.adaptor;

import android.arch.persistence.room.ColumnInfo;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DELL on 26-11-2017.
 */

public class NoteModel implements Parcelable {

    private int noteId;
    private String title;
    private String description;
    private String updateTime;
    private boolean isStared;
    private boolean isFavourite;
    private boolean poemTag;
    private boolean storyTag;

    public NoteModel() {
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isStared() {
        return isStared;
    }

    public void setStared(boolean stared) {
        isStared = stared;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public boolean isPoemTag() {
        return poemTag;
    }

    public void setPoemTag(boolean poemTag) {
        this.poemTag = poemTag;
    }

    public boolean isStoryTag() {
        return storyTag;
    }

    public void setStoryTag(boolean storyTag) {
        this.storyTag = storyTag;
    }


    protected NoteModel(Parcel in) {
        noteId = in.readInt();
        title = in.readString();
        description = in.readString();
        updateTime = in.readString();
        isStared = in.readByte() != 0x00;
        isFavourite = in.readByte() != 0x00;
        poemTag = in.readByte() != 0x00;
        storyTag = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(noteId);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(updateTime);
        dest.writeByte((byte) (isStared ? 0x01 : 0x00));
        dest.writeByte((byte) (isFavourite ? 0x01 : 0x00));
        dest.writeByte((byte) (poemTag ? 0x01 : 0x00));
        dest.writeByte((byte) (storyTag ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<NoteModel> CREATOR = new Parcelable.Creator<NoteModel>() {
        @Override
        public NoteModel createFromParcel(Parcel in) {
            return new NoteModel(in);
        }

        @Override
        public NoteModel[] newArray(int size) {
            return new NoteModel[size];
        }
    };
}