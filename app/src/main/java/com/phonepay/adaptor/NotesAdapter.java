package com.phonepay.adaptor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.phonepay.R;
import com.phonepay.database.Note;
import com.phonepay.fragments.LandingFragment;
import com.phonepay.utils.Utilities;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by DELL on 25-11-2017.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {

    private AdapterInterface mAdapterInterface;
    private List<Note> notesList = new ArrayList<>();

    public NotesAdapter(List<Note> notes, LandingFragment landingFragment) {
        this.notesList = notes;
        this.mAdapterInterface = landingFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.title.setText(notesList.get(holder.getAdapterPosition()).getTitle());
        holder.des.setText(notesList.get(holder.getAdapterPosition()).getDescription());

        if (notesList.get(holder.getAdapterPosition()).getUpdateTime() != null) {
            holder.lastUpdate.setText(Utilities.getDate(
                    Long.parseLong(notesList.get(holder.getAdapterPosition()).getUpdateTime())));
        }

        holder.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapterInterface.deleteData(notesList.get(holder.getAdapterPosition()));
                notesList.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());

                if (notesList.size() == 0) {
                    mAdapterInterface.showCreateNoteContainer();
                }

            }
        });

        holder.noteContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapterInterface.openDisplayPage(notesList.get(holder.getAdapterPosition()));
            }
        });

        setLikeClick(notesList.get(holder.getAdapterPosition()).isFavourite(), holder.heartImg);
        setStarClick(notesList.get(holder.getAdapterPosition()).isStared(), holder.starImg);

        holder.starImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapterInterface.updateData(notesList.get(holder.getAdapterPosition()));
                notesList.get(holder.getAdapterPosition())
                        .setStared(!notesList.get(holder.getAdapterPosition()).isStared());

                setStarClick(notesList.get(holder.getAdapterPosition()).isStared(), holder.starImg);
            }
        });

        holder.heartImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapterInterface.updateData(notesList.get(holder.getAdapterPosition()));
                notesList.get(holder.getAdapterPosition())
                        .setFavourite(!notesList.get(holder.getAdapterPosition()).isFavourite());

                setLikeClick(notesList.get(holder.getAdapterPosition()).isFavourite(), holder.heartImg);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (notesList != null && notesList.size() > 0) {
            return notesList.size();
        } else {
            return 0;
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title, des, lastUpdate;
        private ImageView starImg, heartImg;
        private View deleteLayout;
        private LinearLayout noteContainer;

        private MyViewHolder(View itemView) {
            super(itemView);
            bindData(itemView);
        }

        private void bindData(View itemView) {
            deleteLayout = itemView.findViewById(R.id.delete_layout);
            title = itemView.findViewById(R.id.title_tv);
            des = itemView.findViewById(R.id.des_tv);
            lastUpdate = itemView.findViewById(R.id.time_tv);
            starImg = itemView.findViewById(R.id.star_iv);
            heartImg = itemView.findViewById(R.id.heart_iv);
            noteContainer = itemView.findViewById(R.id.note_container);
        }
    }

    private void setLikeClick(boolean isSelected, ImageView imageView) {
        if (isSelected) {
            setIcon(imageView, R.drawable.ic_heart_selected);
        } else {
            setIcon(imageView, R.drawable.ic_heart_unselected);
        }
    }

    private void setStarClick(boolean isSelected, ImageView imageView) {
        if (isSelected) {
            setIcon(imageView, R.drawable.ic_star_selected);
        } else {
            setIcon(imageView, R.drawable.ic_star_unselected);
        }
    }

    private void setIcon(ImageView imageView, int drawable) {
        imageView.setImageResource(drawable);
    }

    public interface AdapterInterface {

        void openDisplayPage(Note note);

        void updateData(Note note);

        void deleteData(Note note);

        void showCreateNoteContainer();

    }

}