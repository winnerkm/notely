package com.phonepay.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by DELL on 26-11-2017.
 */

@Entity
public class Note {
    @PrimaryKey(autoGenerate = true)
    private int noteId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "update_time")
    private String updateTime;

    @ColumnInfo(name = "is_stared")
    private boolean isStared;

    @ColumnInfo(name = "is_favourite")
    private boolean poemTag;

    @ColumnInfo(name = "poem_tag")
    private boolean storyTag;

    @ColumnInfo(name = "story_tag")
    private boolean isFavourite;

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isStared() {
        return isStared;
    }

    public void setStared(boolean stared) {
        isStared = stared;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public boolean isPoemTag() {
        return poemTag;
    }

    public void setPoemTag(boolean poemTag) {
        this.poemTag = poemTag;
    }

    public boolean isStoryTag() {
        return storyTag;
    }

    public void setStoryTag(boolean storyTag) {
        this.storyTag = storyTag;
    }
}
