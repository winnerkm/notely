package com.phonepay.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by DELL on 26-11-2017.
 */

@Dao
public interface NoteDao {

    @Query("SELECT * FROM note")
    List<Note> getAll();

    @Insert(onConflict = REPLACE)
    void insertNote(Note note);

    @Update(onConflict = REPLACE)
    void updateRecord(Note note);

    @Delete
    void delete(Note note);

}
