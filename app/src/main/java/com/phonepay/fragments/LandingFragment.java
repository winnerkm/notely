package com.phonepay.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.phonepay.R;
import com.phonepay.adaptor.NotesAdapter;
import com.phonepay.database.AppDatabase;
import com.phonepay.database.Note;
import com.phonepay.utils.Constants;
import com.phonepay.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by DELL on 26-11-2017.
 */

public class LandingFragment extends Fragment implements NotesAdapter.AdapterInterface {

    public static final String TAG = LandingFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;
    private AppDatabase db;
    private int mLoadDataFlag = Constants.ALL_NOTES;
    private int mTagFlag;
    private List<Note> mNoteList = new ArrayList<>();

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @OnClick(R.id.no_notes_container)
    void onNoNotesFoundContainerClick() {
        mListener.openCreateFrag();
    }

    @BindView(R.id.no_notes_container)
    LinearLayout noNotesFound;

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);

        if (getArguments() != null && getArguments().containsKey(Constants.LOAD_DATA_FLAG)) {
            mLoadDataFlag = getArguments().getInt(Constants.LOAD_DATA_FLAG);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.FILTER_TAG_)) {
            mTagFlag = getArguments().getInt(Constants.FILTER_TAG_);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (container != null) container.removeAllViews();
        View rootView = inflater.inflate(R.layout.fragment_landing, container, false);
        ButterKnife.bind(this, rootView);
        db = Room.databaseBuilder(getActivity(), AppDatabase.class, AppDatabase.DB_NAME).build();
        initRecyclerView();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadNotes();
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    private void setData() {

        if (mNoteList != null && mNoteList.size() > 0) {

            ArrayList<Note> notesList = Utilities.getFilteredNotes(mLoadDataFlag, mTagFlag, mNoteList);

            NotesAdapter mAdapter = new NotesAdapter(notesList, this);
            mRecyclerView.setAdapter(mAdapter);

        } else {

            noNotesFound.setVisibility(View.VISIBLE);

        }
    }

    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachToContext(context);
    }

    /*
    * Deprecated on API 23
    * Use onAttachToContext instead
    */

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            onAttachToContext(activity);
        }
    }

    /*
     * Called when the fragment attaches to the context
     */
    protected void onAttachToContext(Context context) {
        Activity activity = (Activity) context;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void openDisplayPage(Note note) {
        mListener.openDisplayFrag(note);
    }

    @Override
    public void updateData(Note note) {
        new DataBaseAsyncUpdate().execute(note);
    }

    @Override
    public void deleteData(Note note) {
        new DataBaseAsyncDelete().execute(note);
    }

    @Override
    public void showCreateNoteContainer() {
        noNotesFound.setVisibility(View.VISIBLE);
    }


    @SuppressLint("StaticFieldLeak")
    private class DataBaseAsyncUpdate extends AsyncTask<Note, Void, Void> {

        @Override
        protected Void doInBackground(Note... notes) {
            db.noteDao().updateRecord(notes[0]);
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class DataBaseAsyncDelete extends AsyncTask<Note, Void, Void> {

        @Override
        protected Void doInBackground(Note... notes) {
            db.noteDao().delete(notes[0]);
            return null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void loadNotes() {
        new AsyncTask<Void, Void, List<Note>>() {
            @Override
            protected List<Note> doInBackground(Void... params) {
                mNoteList = db.noteDao().getAll();
                return mNoteList;
            }

            @Override
            protected void onPostExecute(List<Note> notes) {
                setData();
            }
        }.execute();

    }

    public interface OnFragmentInteractionListener {
        void openDisplayFrag(Note note);

        void openCreateFrag();
    }

}
