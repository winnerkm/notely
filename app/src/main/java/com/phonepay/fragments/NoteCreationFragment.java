package com.phonepay.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.phonepay.R;
import com.phonepay.adaptor.NoteModel;
import com.phonepay.database.Note;
import com.phonepay.utils.Constants;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Dell on 25/11/17.
 */

public class NoteCreationFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public static final String TAG = NoteCreationFragment.class.getSimpleName();

    private NoteModel mNote;
    private boolean mIsEditFlow = false;

    /****** Title and description binding ******/

    @BindView(R.id.title_edit_text)
    EditText mTitleEditText;
    @BindView(R.id.content_edit_text)
    EditText mContentEditText;


    /*********** Tag Binding *******************/

    @BindView(R.id.poem_tag)
    TextView poemTagTV;

    private boolean poemTag;

    @OnClick(R.id.poem_tag)
    void onPoemTagClick() {
        poemTag = !poemTag;
        if (poemTag) {
            poemTagTV.setAlpha(1);
        } else {
            poemTagTV.setAlpha((float) 0.6);
        }
    }

    @BindView(R.id.story_tag)
    TextView storyTagTV;

    private boolean storyTag = false;

    @OnClick(R.id.story_tag)
    void onStoryTagClick() {
        storyTag = !storyTag;
        if (storyTag) {
            storyTagTV.setAlpha(1);
        } else {
            storyTagTV.setAlpha((float) 0.6);
        }
    }

    /*********    Toolbar Binding  **********/

    @BindView(R.id.title1_tv)
    TextView undoBtn;

    @OnClick(R.id.back_btn)
    void onBackBtnClick() {
        getActivity().finish();
    }

    @OnClick(R.id.title1_tv)
    void onUndoBtnClick() {
        setData();
    }

    @OnClick(R.id.title2_tv)
    void onSaveBtnClick() {
        Note note = new Note();
        if (mNote != null) {
            note.setNoteId(mNote.getNoteId());
        }
        note.setDescription(mContentEditText.getText().toString().trim());
        note.setTitle(mTitleEditText.getText().toString().trim());
        long time = Calendar.getInstance().getTimeInMillis();
        note.setUpdateTime(String.valueOf(time));

        note.setPoemTag(poemTag);
        note.setStoryTag(storyTag);

        mListener.saveData(note, mIsEditFlow);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.NOTE_MODEL)) {
                mNote = getArguments().getParcelable(Constants.NOTE_MODEL);
            }
            if (getArguments().containsKey(Constants.FRAG_MODE)) {
                mIsEditFlow = getArguments().getBoolean(Constants.FRAG_MODE, false);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (container != null) container.removeAllViews();
        View rootView = inflater.inflate(R.layout.fragment_create_note, container, false);
        ButterKnife.bind(this, rootView);
        setData();
        return rootView;
    }

    private void setData() {
        if (mIsEditFlow) {
            mTitleEditText.setText(mNote.getTitle());
            mContentEditText.setText(mNote.getDescription());
            if (mNote.isStoryTag()) {
                onStoryTagClick();
            }
            if (mNote.isPoemTag()) {
                onPoemTagClick();
            }
        } else {
            undoBtn.setVisibility(View.GONE);
        }
    }


    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachToContext(context);
    }

    /*
    * Deprecated on API 23
    * Use onAttachToContext instead
    */

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            onAttachToContext(activity);
        }
    }

    /*
     * Called when the fragment attaches to the context
     */
    protected void onAttachToContext(Context context) {
        Activity activity = (Activity) context;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void saveData(Note note, boolean isEditMode);
    }
}
