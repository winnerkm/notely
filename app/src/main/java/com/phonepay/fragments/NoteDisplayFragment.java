package com.phonepay.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.phonepay.R;
import com.phonepay.adaptor.NoteModel;
import com.phonepay.database.Note;
import com.phonepay.utils.Constants;
import com.phonepay.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Dell on 26/11/17.
 */

public class NoteDisplayFragment extends Fragment {

    public static final String TAG = NoteDisplayFragment.class.getSimpleName();

    private NoteModel mNote;
    private OnFragmentInteractionListener mListener;

    @BindView(R.id.title_text_view)
    TextView mTitleTextView;
    @BindView(R.id.content_text_view)
    TextView mContentTextView;
    @BindView(R.id.last_updated_text_view)
    TextView mLastUpdatedOnTextView;


    // Toolbar Data Binding

    @BindView(R.id.title1_tv)
    TextView title1Tv;

    @BindView(R.id.title2_tv)
    TextView title2Tv;

    @OnClick(R.id.back_btn)
    void onBackBtnClick() {
        getActivity().finish();
    }

    @OnClick(R.id.title2_tv)
    void onEditBtnClick() {
        mListener.onEditClick(mNote);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Constants.NOTE_MODEL)) {
            mNote = getArguments().getParcelable(Constants.NOTE_MODEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_display_note, container, false);
        ButterKnife.bind(this, rootView);
        setData();
        setupToolbar();
        return rootView;
    }

    private void setupToolbar() {
        title1Tv.setVisibility(View.GONE);
        title2Tv.setText(getString(R.string.edit));
    }

    private void setData() {
        if (mNote != null) {
            mTitleTextView.setText(mNote.getTitle());
            mContentTextView.setText(mNote.getDescription());
            mLastUpdatedOnTextView.setText("Last updated " +
                    Utilities.getDate(Long.parseLong(mNote.getUpdateTime())));
        }
    }

    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachToContext(context);
    }

    /*
    * Deprecated on API 23
    * Use onAttachToContext instead
    */

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            onAttachToContext(activity);
        }
    }

    /*
     * Called when the fragment attaches to the context
     */
    protected void onAttachToContext(Context context) {
        Activity activity = (Activity) context;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onEditClick(NoteModel noteModel);
    }
}
