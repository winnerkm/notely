package com.phonepay.utils;

/**
 * Created by DELL on 26-11-2017.
 */

public class Constants {

    public static final String CREATE = "create";
    public static final String DISPLAY = "display";
    public static final String NOTE_MODEL = "note_model";
    public static final String FRAG_MODE = "frag_mode";
    public static final String FRAG_TYPE = "frag_type";

    public static final int FAV_NOTES = 1;
    public static final int STARED_NOTES = 2;
    public static final int FAV_AND_STARED_NOTES = 3;
    public static final int ALL_NOTES = 4;
    public static final int POEM_TAG_NOTES = 5;
    public static final int STORY_TAG_NOTES = 6;
    public static final int POEM_AND_STORY_TAG_NOTES = 6;
    public static final int TAG_NONE = 7;

    public static final String LOAD_DATA_FLAG = "load_data_flag";
    public static final String FILTER_TAG_ = "filter_tag";

}
