package com.phonepay.utils;

import com.phonepay.adaptor.NoteModel;
import com.phonepay.database.Note;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by DELL on 26-11-2017.
 */

public class Utilities {

    public static String getDate(long timeStamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh.mm a");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }

    public static NoteModel convertNoteData(Note mNote) {
        NoteModel noteModel = new NoteModel();
        noteModel.setTitle(mNote.getTitle());
        noteModel.setDescription(mNote.getDescription());
        noteModel.setNoteId(mNote.getNoteId());
        noteModel.setStared(mNote.isStared());
        noteModel.setUpdateTime(mNote.getUpdateTime());
        noteModel.setFavourite(mNote.isFavourite());
        noteModel.setPoemTag(mNote.isPoemTag());
        noteModel.setStoryTag(mNote.isStoryTag());
        return noteModel;
    }

    private static ArrayList<Note> favList(List<Note> noteList) {

        ArrayList<Note> favList = new ArrayList<>();

        for (Note note : noteList) {
            if (note.isFavourite()) {
                favList.add(note);
            }
        }
        return favList;
    }

    private static ArrayList<Note> staredList(List<Note> noteList) {

        ArrayList<Note> staredList = new ArrayList<>();

        for (Note note : noteList) {
            if (note.isStared()) {
                staredList.add(note);
            }
        }
        return staredList;
    }

    public static ArrayList<Note> getFilteredNotes(int loadDataFlag, int tagType, List<Note> mNoteList) {

        ArrayList<Note> noteList = new ArrayList<>();

        if (loadDataFlag == Constants.FAV_AND_STARED_NOTES) {
            noteList.addAll(Utilities.favAndStaredList(mNoteList));
        } else if (loadDataFlag == Constants.FAV_NOTES) {
            noteList.addAll(Utilities.favList(mNoteList));
        } else if (loadDataFlag == Constants.STARED_NOTES) {
            noteList.addAll(Utilities.staredList(mNoteList));
        } else {
            noteList = (ArrayList<Note>) mNoteList;
        }

        return filterListByTag(tagType, noteList);

    }

    private static ArrayList<Note> favAndStaredList(List<Note> noteList) {
        ArrayList<Note> notes = new ArrayList<>();

        for (Note note : noteList) {
            if (note.isStared() && note.isFavourite()) {
                notes.add(note);
            }
        }
        return notes;
    }

    private static ArrayList<Note> filterListByTag(int tagType, ArrayList<Note> noteList) {

        ArrayList<Note> filteredNoteList = new ArrayList<>();

        if (tagType == Constants.POEM_TAG_NOTES) {

            for (Note note : noteList) {
                if (note.isPoemTag())
                    filteredNoteList.add(note);
            }
        } else if (tagType == Constants.STORY_TAG_NOTES) {

            for (Note note : noteList) {
                if (note.isStoryTag())
                    filteredNoteList.add(note);
            }
        } else {
            filteredNoteList = noteList;
        }

        return filteredNoteList;

    }
}
